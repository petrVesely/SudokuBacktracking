import csv
from dev.Backtracking.SudokuRecursiveBacktracking import sudokuSolver, printBoard


class SudokuBoard:

    def __init__(self):
        self.sudoku = [[0 for x in range(9)] for x in range(9)]
        self.solvedSudoku = [[0 for x in range(9)] for x in range(9)]

    def getBoard(self):
        return self.sudoku

    def setBoard(self, board):
        self.sudoku = board



    def getSolved(self):
        return self.solvedSudoku

    def setSolved(self, solved):
        self.solvedSudoku = solved


    def getValue(self, x, y):
        return self.sudoku[x][y]


    def setValue(self, x, y, val):
        self.sudoku[x][y] = val



    def checkValue(self, val):
        while True:
            if val <1 or val >9:
                print("That value was incorrect")
                val = int(input("Enter a Value"))
                continue
            else:
                return val

    def displayBoard(self):
        for row in self.sudoku:
            print(row)


    # This Function Reads A csv File and writes the
    # Values to the self.suduko Array, the changed array
    # Is then returned
    def readSudoku(self, fileName):
        y = 0
        with open(fileName, mode="r") as myFile:
            # Loop for each row
            for i in range(0, 9):
                x = i
                for j in myFile.readline():
                    # Ignore commas and newlines
                    if j == ",":
                        continue
                    if j == "\n":
                        continue
                    else:
                        self.sudoku[x][y] = int(j)
                        y += 1
                        y = y % 9


        return self.sudoku

    # Not working - Don't know why the value for board is not returned from module
    def writeSudoku(self, fileName, solvedBoard):
        with open(fileName, mode="w") as myFile:
            myFileWriter = csv.writer(myFile)
            for row in solvedBoard:
                myFileWriter.writerow(row)
            print("The File Was Saved as", fileName)







board = SudokuBoard()
board.readSudoku("Sudoku1Unsolved.csv")
solvedBoard = sudokuSolver(board.getBoard())
printBoard(solvedBoard)

