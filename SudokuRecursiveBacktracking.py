import csv
# This function is used to check if the board is full on not
# Return True if it is full and False if it isn't
# It works on the fact that if it finds at least one zero
# it the board it returns False


def isFull(board):
    for x in range(9):
        for y in range(9):
            if board[x][y] == 0:
                return False
    return True


# Function to find all of the possible numbers
# Which can be put at a specific cell on the board
# this is done by checking the horizontal and vertical
# as well as 3x3 square in which the numbers are found

def possibleEntries(board, i, j):
    possiblilityArray = {}  # Dictionary is used for values 1-9
    # Initialising the Dictionary
    for x in range(1,10):
        possiblilityArray[x] = 0

    # For Horizontal Entries

    for y in range(0,9):
        if board[i][y] != 0:
            possiblilityArray[board[i][y]] = 1  # Index value with 0 represents a possibility,
                                                # The index of the dictianry represents the value

    # For Vertical Entries

    for x in range(9):
        if board[x][j] != 0:
            possiblilityArray[board[x][j]] = 1

    # For squares of 3x3

    k = 0
    l = 0

    # Get the locations of each box
    if i >= 0 and i <= 2:
        k = 0
    elif i >= 3 and i <= 5:
        k = 3
    else:
        k = 6

    if j >= 0 and j <= 2:
        l = 0
    elif j >= 3 and j <= 5:
        l = 3
    else:
        l = 6
    # Check each box
    for x in range(k, k+3):
        for y in range(l, l+3):
            if board[x][y] != 0:
                possiblilityArray[board[x][y]] = 1


    for x in range(1, 10):
        if possiblilityArray[x] == 0:
            possiblilityArray[x] = x
        else:
            possiblilityArray[x] = 0

    return possiblilityArray

# Recursive Function which solved the board
# Prints it

def sudokuSolver(board):

    i = 0
    j = 0
    new_board = False
    possibilities = {}  # Another Dictionary

    # If board is full, there is no need to solve it any further
    # Base Case - Recursive Functions Must Have A Base Case in order to stop
    if isFull(board) == True:
        #print("Board Solved Successfully")
        #printBoard(board)
        writeToFile("SolvedPuzzle.csv", board)
        return board
    else:
        # Find the first vacant spot
        for y in range(9):
            for x in range(9):
                if board[x][y] == 0:
                    i = x  # Co-ordinates of the vacant spot
                    j = y
                    break
                else:
                    continue
                break
        # Get all the possible values for the spot with coordinate i, j
        possibilities = possibleEntries(board, i, j)

    # Go through all the possibilities and call the function again and again
    for x in range(1,10):
        if possibilities[x] != 0:
            board[i][j] = possibilities[x]
            new_board = sudokuSolver(board)  # The function is run again with the new input
    # Backtracking Step - This resets the previous spot to zero, goes back as far until you have more possibilites
    if (not new_board):
        board[i][j] = 0
    return new_board

def printBoard(board):
    for x in board:
        print(x)

def writeToFile(fileName, solvedBoard):
    with open(fileName, mode="w") as myFile:
            for x in range(9):
                for y in range(9):
                    myFile.write(str(solvedBoard[x][y])+ ",")
                myFile.write("\n")
